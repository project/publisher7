<?php
/**
 * @file
 * fe_components.features.inc
 */

/**
 * Implements hook_views_api().
 */
function fe_components_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
